import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const API_URL = 'http://localhost:3000'
axios.defaults.baseURL = API_URL

const state = {
  loading: true,
  currentLabelId: undefined,
  currentMemoId: undefined,
  memos: [],
  labels: []
}

const getters = {
  memosMap: state => {
    let resultMap = {}
    state.memos.forEach(element => {
      resultMap[element._id] = element
    })
    return resultMap
  },
  labelsMap: state => {
    let resultMap = {}
    state.labels.forEach(element => {
      resultMap[element._id] = element
    })
    return resultMap
  },
  memosInLabel: (state, getters) => {
    if (state.currentLabelId === undefined || state.currentLabelId === '0') {
      return state.memos
    } else {
      return getters.labelsMap[state.currentLabelId].memos
    }
  }
}

const mutations = {
  SET_LOADING (state, flag) {
    state.loading = flag
  },
  SET_LABEL_ID (state, labelId) {
    state.currentLabelId = labelId
  },
  SET_MEMO_ID (state, memoId) {
    state.currentMemoId = memoId
  },
  SET_MEMOS (state, memos) {
    state.memos = memos
  },
  SET_LABELS (state, labels) {
    state.labels = labels
  },
  ADD_LABEL (state, labelObject) {
    console.log('push label', labelObject)
    state.labels.push(labelObject)
  },
  ADD_MEMO (state, memoObject) {
    console.log('push memo', memoObject)
    state.memos.push(memoObject)
  },
  UPDATE_MEMO (state, memoObject) {
    const idx = state.memos.findIndex(e => e._id === memoObject._id)
    if (idx !== -1) {
      state.memos.splice(idx, 1, memoObject)
    }
  },
  REMOVE_MEMO (state, memoObject) {
    const memos = state.memos
    memos.splice(memos.indexOf(memoObject), 1)
  },
  ADD_MEMO_TO_LABEL (state, {memoObj, labelId}) {
    console.log('push memo to label ', memoObj, labelId)
    const idx = state.labels.findIndex(e => e._id === labelId)
    if (idx !== -1) {
      state.labels.splice(idx, 1, memoObj)
    }
  }
}

const actions = {
  loadMemos ({ commit, dispatch }) {
    commit('SET_LOADING', true)
    return axios
      .get('/memos')
      .then(r => r.data)
      .then(memos => {
        commit('SET_MEMOS', memos)
        if (memos.length === 0) {
          // TODO: 메모가 하나도 없으면 화면이 표시안되는 문제있음
          dispatch('addMemo', { newMemo: 'test memo' })
        } else {
          dispatch('loadLabels')
            .then(_ => {
              commit('SET_LOADING', false)
            })
        }
      })
  },
  loadLabels ({ commit }) {
    return axios
      .get('/labels', { baseURL: API_URL })
      .then(r => r.data)
      .then(labels => {
        commit('SET_LABELS', labels)
      })
  },
  addLabel ({ commit, state }, newLabel) {
    const data = {
      title: newLabel
    }
    return axios.post('/labels', data, { baseURL: API_URL })
      .then(r => {
        commit('ADD_LABEL', r.data)
      })
  },
  addMemo ({ commit, state, getters }, { newMemo, labelId }) {
    const data = {
      title: newMemo
    }
    return axios.post('/memos', data, { baseURL: API_URL })
      .then(r => {
        commit('ADD_MEMO', r.data)

        // add memo to label
        if (labelId !== undefined && labelId !== '0') {
          const data = {
            memoIds: [ r.data._id ]
          }
          axios.post(`/labels/${labelId}/memos`, data, { baseURL: API_URL })
            .then(r => {
              commit('ADD_MEMO_TO_LABEL', {
                memoObj: r.data,
                labelId
              })
            })
        }
      })
  },
  updateMemo ({ dispatch, commit, state, getters }, { memoId, title, content }) {
    const data = {
      title,
      content
    }
    return axios.put(`/memos/${memoId}`, data, { baseURL: API_URL })
      .then(r => {
        commit('UPDATE_MEMO', r.data)
        dispatch('loadLabels')
      })
  },
  removeMemo ({ dispatch, commit, state, getters }, { memoId }) {
    return axios.delete(`/memos/${memoId}`, { baseURL: API_URL })
      .then(r => {
        commit('REMOVE_MEMO', r.data)
        dispatch('loadLabels')
      })
  },
  addMemoToLabel ({ commit, state }, { _id }, labelId) {
    const data = {
      memoIds: [ _id ]
    }
    return axios.post(`/labels/${labelId}/memos`, data, { baseURL: API_URL })
      .then(r => {
        commit('ADD_MEMO_TO_LABEL', _id, labelId)
      })
  }
}

const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})

export default store
