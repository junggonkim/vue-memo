# vue-memo

> A Vue.js simple memo project
  - Frontend library : Vue.js
    - vue-router의 동적라우팅을 이용하여 label, memo 간 전환 구현
    - 전체 memo와 label 들은 Vuex 로 상태관리(Redux, Flux 의 Vue 구현체)
  - UI framework : [Element UI](https://element.eleme.io/#/en-US)
  - Coding style : [Standard rule](https://github.com/standard/standard/blob/master/docs/RULES-en.md)

![snapshot](snapshot.png)

## Prerequisites

 [MemoApp API](https://github.com/dramancompany/memoapp-api)

> Clone and run [MemoApp API](https://github.com/dramancompany/memoapp-api)
*(MemoApp API service 는 localhost:3000 에서 실행되어야 함)*

> Enable CORS ( 2 options )
  1. On MemoApp API service
   - Install cors library
  ```bash
  npm install cors -S
  ```
   - Add related code in app.js
  ```js
  ...
  var app = express();

  // added lines
  app.use(cors({
    origin: 'http://localhost:8080',
    credentials: true
  }));
  ```
   - Restart MemoApp API service

  2. or on Chrom browser : Install [Chrome CORS plugin](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi) for quick test


## URL 라우팅
 - / - 모든 레이블 목록과 전체 메모 목록 보임
 - /labelId - 선택된 레이블의 메모 목록만 보임
 - /labelId/memoId - 선택한 메모가 보임
 - /0 - 전체메모를 보기 위한 labelId

## TODO 남은 것들
 - 메모 여러개 선택해서 삭제하기
 - 레이블 이름 변경, 삭제
 - unit, e2e test 코드

## 문제점
 - db 에 메모가 하나도 없을 경우 메인 화면이 표시가 안되는 문제
 (임시로 메모 하나를 만들어서 임시해결)

## 개선 사항
대략
> 개발자용
 - markdown(개발자용)
 - gist 에 올리기
> 일반 사용자용
 - rich text editor
 - evernote 처럼 기능 확장하기 ?
> 공통
 - 링크만들어서 공유하기(short url 제공)
 - sns 처럼 댓글 달기

## 설치 및 실행하기

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

## 기타 명령어

``` bash
# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```
