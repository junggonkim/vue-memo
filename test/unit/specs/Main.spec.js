import Vue from 'vue'
import ElementUI from 'element-ui';    
import Main from '@/components/Main'

Vue.use(ElementUI)

describe('Main.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Main)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.hello h1').textContent)
      .to.equal('memos')
  })
})
